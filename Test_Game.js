//Import library
var webdriver = require('selenium-webdriver');
var test = require('selenium-webdriver/testing');
var chrome = require('selenium-webdriver/chrome');

//Variables for the game you are looking for and url
var searchGame = 'Mayfair Roulette';
var URL = 'https://vegas.williamhill.com';

//Test description and function declaration
test.describe("SearchTest", function () {

    //Defining the driver variable
    var driver;
    //Global timeout for test
    this.timeout(20000);

    //Test elements which start before test(Chrome browser start)
    test.before(function () {
        driver = new webdriver.Builder()
                .forBrowser('chrome')
                .build();
        //Maximize the browser window
        driver.manage().window().maximize();
    });
    //Test elements which start after test
    test.after(function () {
        driver.close();
        driver.quit();
    });

    //Steps of test
    test.it("Testcase1", function (done) {
        //open web
        driver.get(URL);
        //click on magnifier ( upper right corner)
        driver.findElement(webdriver.By.className("fa fa-search")).click();
        //input test to search ( name of game)
        driver.findElement(webdriver.By.css("input[type='text']")).sendKeys(searchGame);
        //Mouse override for tiles with game
        var game = webdriver.By.xpath("//h4[.='" + searchGame + "']");
        driver.wait(webdriver.until.elementLocated(game));
        driver.actions().mouseMove(driver.findElement(game)).click();
        done()
        });
});